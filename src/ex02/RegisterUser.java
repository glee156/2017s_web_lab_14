package ex02;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Created by mshe666 on 11/01/2018.
 */
public class RegisterUser extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession sess = req.getSession(true);
        String sessiont_id = sess.getId();
        String fileName = "H:\\Documents\\COMPSCI_719\\2017s_web_lab_14\\" + sessiont_id + ".json";
        File sessionFile = new File(fileName);

        String fname = "";
        String lname = "";
        String userName = "";
        String email = "";

        if (sessionFile.exists()) {
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(sessionFile))) {
                String line = null;
                Map<String, String[]> map = new HashMap<>();

                while ((line = bufferedReader.readLine()) != null) {

                    JSONParser parser = new JSONParser();
                    Object obj = parser.parse(line);
                    JSONObject jsonObject = (JSONObject) obj;


                    fname = (String)jsonObject.get("firstName");
                    lname = (String)jsonObject.get("lastName");
                    userName = (String)jsonObject.get("userName");
                    email = (String)jsonObject.get("email");
                }

            }catch (IOException e) {
                e.getStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        out.println("<!DOCTYPE html>");
        out.println("<html lang=\"en\">");
        out.println("<head>");
        out.println("<meta charset=\"UTF-8\">");
        out.println("<title>Register user</title>");
        out.println("</head>");
        out.println("<body>");

        // Print out a form allowing users to enter new cookies
        //out.println("<form action=\"Cookies\" method=\"GET\">");
        out.println("<form action=\"savedSuccessfully\" method = \"GET\">");
        out.println("<p>Welcome to your registration!</p>");
        out.println("<label for=\"inputFirstName\">First name:</label>");
        out.println("<input type=\"text\" id=\"inputFirstName\" name=\"firstName\" minLength=\"1\" value=\""+ fname + "\"/>");
        out.println("<br>");
        out.println("<label for=\"inputLastName\">Last name:</label>");
        out.println("<input type=\"text\" id=\"inputLastName\" name=\"lastName\" minLength=\"1\" value=\""+ lname + "\"/>");
        out.println("<br>");
//        out.println("<input type=\"hidden\" name=\"option\" value=\"create\" />");
        out.println("<label for=\"inputUserName\">User name:</label>");
        out.println("<input type=\"text\" id=\"inputUserName\" name=\"userName\" maxLength=\"6\" value=\""+ userName + "\"/>");
        out.println("<br>");
        out.println("<label for=\"inputEmail\">Email address:</label>");
        out.println("<input type=\"email\" id=\"inputEmail\" name=\"email\" value=\""+ email + "\"/>");
        out.println("<br>");
        out.println("<input type=\"submit\"/ value=\"Register\">");
        out.println("</form>");



    }
}
