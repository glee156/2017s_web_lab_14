package ex02;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mshe666 on 11/01/2018.
 */
public class SaveUserInfo extends HttpServlet {

    private String session_id;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("in SaveUserInfo.doGet");

        Map<String, String[]> map = req.getParameterMap();

        String fname = null;
        String lname = null;
        String userName = null;
        String email = null;
        boolean empty = false;


        for (String key : map.keySet()) {
            String[] value = map.get(key);
            if (value[0].equals("")) {empty = true;}

            switch (key) {
                case "firstName":
                    fname = value[0];
                case "lastName":
                    lname = value[0];
                case "userName":
                    userName = value[0];
                case "email":
                    email = value[0];
            }
        }

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        out.println("<!DOCTYPE html>");
        out.println("<html lang=\"en\">");
        out.println("<head>");
        out.println("<meta charset=\"UTF-8\">");
        out.println("<title>Register user</title>");
        out.println("</head>");
        out.println("<body>");


        if (!empty) {
            out.println("<form>");
            displayForm(out, fname, lname, userName, email, true);
            out.println("</form>");
        }else{
            out.println("<form>");
            displayForm(out, fname, lname, userName, email, false);
            out.println("</form>");
            out.println("To continue your registration,<a href=\"/ex02/registerUser\">click here</a>");
        }

        saveUserInfo(req, resp);
    }

    protected void saveUserInfo(HttpServletRequest req, HttpServletResponse resp) {

        Map<String, String[]> map = req.getParameterMap();
        Map<String, String> jsonMap = new HashMap<>();
        for (String key : map.keySet()) {
            String[] value = map.get(key);
            String valueStr = "";
            for (int i = 0; i < value.length; i++) {
                valueStr += value[i];
            }

            jsonMap.put(key, valueStr);
        }


        String jsonText = JSONValue.toJSONString(jsonMap);

        String session_id = req.getSession().getId();
        this.session_id = session_id;
        File file = new File("H:\\Documents\\COMPSCI_719\\2017s_web_lab_14\\" + session_id + ".json");

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(jsonText);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void displayForm(PrintWriter out, String fname, String lname, String userName, String email, boolean isDisabled) {

        String disabled = "";
        if (isDisabled) {
            disabled = "disabled";
        }

        out.println("<label for=\"inputFirstName\">First name:</label>");
        out.println("<input type=\"text\" id=\"inputFirstName\" name=\"firstName\" minLength=\"1\" placeholder=\"" + fname +"\"" + disabled + ">");
        out.println("<br>");
        out.println("<label for=\"inputLastName\">Last name:</label>");
        out.println("<input type=\"text\" id=\"inputLastName\" name=\"lastName\" minLength=\"1\"  placeholder=\"" + lname +"\"" + disabled + ">");
        out.println("<br>");
        out.println("<label for=\"inputUserName\">User name:</label>");
        out.println("<input type=\"text\" id=\"inputUserName\" name=\"userName\" maxLength=\"6\"  placeholder=\"" + userName + "\"" + disabled + ">");
        out.println("<br>");
        out.println("<label for=\"inputEmail\">Email address:</label>");
        out.println("<input type=\"email\" id=\"inputEmail\" name=\"email\" placeholder=\"" + email + "\"" + disabled + ">");
        out.println("<br>");

    }
}
