package ex01;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by mshe666 on 11/01/2018.
 */
public class HitCounter extends HttpServlet {
    protected int count = 0;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        count++;
        Cookie cookie = new Cookie("count", String.valueOf(count));
//        cookie.setMaxAge(1209600);
        cookie.setPath("/exercise01.html");
        resp.addCookie(cookie);

        Map<String, String[]> parameterMap= req.getParameterMap();
        for (String key : parameterMap.keySet()) {
            String[] value = parameterMap.get(key);
            System.out.println(key);
            System.out.println(Arrays.toString(value));

            if (key.equals("removeCookies")) {
                cookie.setMaxAge(0);
                resp.addCookie(cookie);
                count = 0;
            }
        }

//        System.out.println(req.getParameter("checkbox"));

        resp.sendRedirect(resp.encodeRedirectURL("/exercise01.html"));


    }
}
