package ex03;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.simple.JSONValue;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

public class EnhancedImageGallerySubmit extends HttpServlet {
    private int maxFileSize = 5 * 1024 * 1024;
    private String filePath;
    private String thumbnailFile;
    private String session_id;

    public EnhancedImageGallerySubmit() {
        super();
//        filePath = getServletContext().getInitParameter("file-upload");
        filePath = "H:\\Documents\\COMPSCI_719\\2017s_web_lab_14\\web\\Photos\\";
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try (PrintWriter out = response.getWriter()) {
            out.println("Please use POST mode!");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DiskFileItemFactory factory = new DiskFileItemFactory();

        factory.setRepository(new File("H:\\Documents\\COMPSCI_719\\2017s_web_lab_14\\web\\Photos\\"));

        ServletFileUpload upload = new ServletFileUpload(factory);

        upload.setSizeMax(maxFileSize);

        PrintWriter out = response.getWriter();

        try {
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);

            // Process the uploaded file items
            Iterator i = fileItems.iterator();

            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet upload</title>");
            out.println("</head>");
            out.println("<body>");
            File file;
            while (i.hasNext()) {
                FileItem fi = (FileItem) i.next();
                if (!fi.isFormField()) {
                    // Get the uploaded file parameters
                    String fieldName = fi.getFieldName();
                    String fileName = fi.getName();
                    String contentType = fi.getContentType();
                    boolean isInMemory = fi.isInMemory();
                    long sizeInBytes = fi.getSize();

                    // Write the file
                    String finalName;

                    System.out.println("line 76");
                    session_id = request.getSession().getId();
                    File theDir = new File(session_id);
                    if (!theDir.exists()) {
                        theDir.mkdirs();
                        System.out.println("line 81 " + theDir.exists());
                    }

                    filePath = filePath + session_id + "\\";
                    Path path = Paths.get(filePath);
                    Files.createDirectories(path);

                    if (fileName.lastIndexOf("/") >= 0) {
                        finalName = filePath + fileName.substring(fileName.lastIndexOf("/"));
                    } else {
                        finalName = filePath + fileName.substring(fileName.lastIndexOf("/") + 1);
                    }

                    System.out.println("line 92 " + finalName);
                    file = new File(finalName);
                    fi.write(file);
                    out.println("Uploaded Filename: " + finalName + "<br/>");

                    // thumbnails
                    BufferedImage img = null;
                    try {
                        img = ImageIO.read(new File(finalName));
                        thumbnailFile = finalName.replace(finalName.substring(finalName.lastIndexOf(".")), "_thumbnail.png");
                        System.out.println("line 107 " + thumbnailFile);
                        if (img.getHeight() < 400 && img.getWidth() < 400) {

                            ImageIO.write(img, "png", new File(thumbnailFile));
                        } else {
                            double zoom = Math.max(1.0 * img.getHeight() / 400, 1.0 * img.getWidth() / 400);
                            int type = img.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : img.getType();
                            BufferedImage resizedImage = new BufferedImage((int) (img.getWidth() / zoom), (int) (img.getHeight() / zoom), type);
                            Graphics2D g = resizedImage.createGraphics();
                            g.drawImage(img, 0, 0, (int) (img.getWidth() / zoom), (int) (img.getHeight() / zoom), null);
                            g.dispose();
                            g.setComposite(AlphaComposite.Src);
                            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                            ImageIO.write(resizedImage, "png", new File(thumbnailFile));
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    String fieldName = fi.getFieldName();
                    if (fieldName.equals("desc")) {
                        String desc = fi.getString();
                        out.println("The description is: " + desc + "<br/>");
                    }
                }
            }
            out.println("</body>");
            out.println("</html>");
        } catch (Exception ex) {
            System.out.println(ex);
        }

        File file = new File(thumbnailFile);

        File fi = new File(file.getPath().replace("_thumbnail.png", ".jpg"));
        System.out.println("line 145: " + thumbnailFile);
        System.out.println("line 146: " + file);
        System.out.println("line 147: " + file.getPath().replace("_thumbnail.png", ".jpg"));
        System.out.println("line 148: " + fi);

        out.println("<div style=\"width:200px;\"><fieldset>");
        out.println("<legend>" + file.getName().substring(0, file.getName().lastIndexOf("_")) + "</legend>");
        out.println("<a href=\"/Photos/" + session_id + "/" + fi.getName() + "\">");
        out.println("<img src=\"/Photos/" + session_id + "/" + file.getName() + "\" /></a>");

        out.println("<br/>Size:" + fi.length());
        out.println("</fieldset></div>");


    }
}
